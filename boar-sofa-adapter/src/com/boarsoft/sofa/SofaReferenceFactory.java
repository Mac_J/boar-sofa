package com.boarsoft.sofa;

import org.springframework.beans.factory.FactoryBean;

import com.alipay.sofa.rpc.config.ConsumerConfig;

public class SofaReferenceFactory implements FactoryBean<Object> {
	private volatile Object refObject;
	private Class<?> refClazz;
	private ConsumerConfig<Object> consumerConfig;

	@Override
	public Object getObject() throws Exception {
		if (refObject == null) {
			synchronized (consumerConfig) {
				if (refObject == null) {
					refObject = consumerConfig.refer();
				}
			}
		}
		return refObject;
	}

	@Override
	public Class<?> getObjectType() {
		return refClazz;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public Object getRefObject() {
		return refObject;
	}

	public void setRefObject(Object refObject) {
		this.refObject = refObject;
	}

	public Class<?> getRefClazz() {
		return refClazz;
	}

	public void setRefClazz(Class<?> refClazz) {
		this.refClazz = refClazz;
	}

	public ConsumerConfig<Object> getConsumerConfig() {
		return consumerConfig;
	}

	public void setConsumerConfig(ConsumerConfig<Object> consumerConfig) {
		this.consumerConfig = consumerConfig;
	}

}
