package com.boarsoft.sofa.config;

import com.alipay.sofa.rpc.config.ConsumerConfig;

public interface ConsumerConfigBind {
	@SuppressWarnings("rawtypes")
	void bind(ConsumerConfig config);
}
