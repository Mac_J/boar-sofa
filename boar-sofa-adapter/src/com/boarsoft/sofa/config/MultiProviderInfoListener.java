package com.boarsoft.sofa.config;

import java.util.LinkedList;
import java.util.List;

import com.alipay.sofa.rpc.client.ProviderGroup;
import com.alipay.sofa.rpc.listener.ProviderInfoListener;

public class MultiProviderInfoListener implements ProviderInfoListener {
	protected List<ProviderInfoListener> listeners = new LinkedList<>();

	@Override
	public void addProvider(ProviderGroup pg) {
		for (ProviderInfoListener pil : listeners) {
			pil.addProvider(pg);
		}
	}

	@Override
	public void removeProvider(ProviderGroup pg) {
		for (ProviderInfoListener pil : listeners) {
			pil.removeProvider(pg);
		}
	}

	@Override
	public void updateProviders(ProviderGroup pg) {
		for (ProviderInfoListener pil : listeners) {
			pil.updateProviders(pg);
		}
	}

	@Override
	public void updateAllProviders(List<ProviderGroup> pgs) {
		for (ProviderInfoListener pil : listeners) {
			pil.updateAllProviders(pgs);
		}
	}

	public void add(ProviderInfoListener pil) {
		this.listeners.add(pil);
	}

	public void add(List<ProviderInfoListener> pilLt) {
		this.listeners.addAll(pilLt);
	}

	public List<ProviderInfoListener> getListeners() {
		return listeners;
	}

	public void setListeners(List<ProviderInfoListener> listeners) {
		this.listeners = listeners;
	}
}
