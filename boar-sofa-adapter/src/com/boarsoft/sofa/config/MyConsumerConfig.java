package com.boarsoft.sofa.config;

import com.alipay.sofa.rpc.config.ConsumerConfig;
import com.alipay.sofa.rpc.listener.ProviderInfoListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MyConsumerConfig<T> extends ConsumerConfig<T> {
	private static Logger LOGGER = LoggerFactory.getLogger(MyConsumerConfig.class);
	private static final long serialVersionUID = -3293082137321132940L;

	private MultiProviderInfoListener listener = new MultiProviderInfoListener();
	
	public MyConsumerConfig() {
		super.setProviderInfoListener(listener);
	}

	public ConsumerConfig<T> setProviderInfoListener(ProviderInfoListener pil) {
		if (pil == null) {
			return this;
		}
		if (pil instanceof ConsumerConfigBind) {
			((ConsumerConfigBind) pil).bind(this);
		}
		listener.add(pil);
		return this;
	}

	public MyConsumerConfig<T> addListeners(List<ProviderInfoListenerFactory> listenerFactories) {
		for (ProviderInfoListenerFactory fb : listenerFactories) {
			try {
				ProviderInfoListener pi = fb.getObject(this);
				if (pi instanceof ConsumerConfigBind) {
					((ConsumerConfigBind) pi).bind(this);
				}
				listener.add(pi);
			} catch (Exception e) {
				LOGGER.error("ex:{}", e.getMessage(), e);
			}
		}
		return this;
	}
}
