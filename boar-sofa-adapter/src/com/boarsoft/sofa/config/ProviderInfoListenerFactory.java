package com.boarsoft.sofa.config;

import com.alipay.sofa.rpc.config.ConsumerConfig;
import com.alipay.sofa.rpc.listener.ProviderInfoListener;

public interface ProviderInfoListenerFactory {
    ProviderInfoListener getObject(ConsumerConfig<?> cc);
}
