package com.boarsoft.sofa.converter;

import com.alipay.hessian.generic.model.GenericArray;
import com.boarsoft.bean.ObjectConverter;
import com.boarsoft.rpc.generalize.RpcGenArray;

public class GenArrConverter implements ObjectConverter<GenericArray, RpcGenArray> {

	public static RpcGenArray convert(GenericArray a) throws Exception {
		if (a == null) {
			return null;
		}
		Object[] t = new Object[a.getLength()];
		for (int i = 0; i < a.getLength(); i++) {
			t[i] = GenObjConverter.convert(a.get(i));
		}
		return new RpcGenArray(t);
	}

	public static GenericArray convert(RpcGenArray a) throws Exception {
		if (a == null) {
			return null;
		}
		Object[] t = new Object[a.getLength()];
		for (int i = 0; i < a.getLength(); i++) {
			t[i] = GenObjConverter.convert(a.get(i));
		}
		GenericArray ga = new GenericArray(a.getElClzName());
		ga.setObjects(t);
		return ga;
	}

	@Override
	public RpcGenArray convertAB(GenericArray a) throws Exception {
		return convert(a);
	}

	@Override
	public GenericArray convertBA(RpcGenArray b) throws Exception {
		return convert(b);
	}

}
