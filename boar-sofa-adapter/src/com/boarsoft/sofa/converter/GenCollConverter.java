package com.boarsoft.sofa.converter;

import java.util.Collection;

import com.alipay.hessian.generic.model.GenericCollection;
import com.boarsoft.bean.ObjectConverter;
import com.boarsoft.rpc.generalize.RpcGenCollection;

public class GenCollConverter implements ObjectConverter<GenericCollection, RpcGenCollection> {

	@SuppressWarnings("unchecked")
	public static RpcGenCollection convert(GenericCollection a) throws Exception {
		if (a == null || a.getCollection() == null) {
			return null;
		}
		Collection<Object> tc = a.getCollection().getClass().newInstance();
		for (Object x : a.getCollection()) {
			tc.add(GenObjConverter.convert(x));
		}
		return RpcGenCollection.newInstance(tc);
	}

	@SuppressWarnings("unchecked")
	public static GenericCollection convert(RpcGenCollection a) throws Exception {
		if (a == null || a.getCollection() == null) {
			return null;
		}
		Collection<Object> tc = a.getCollection().getClass().newInstance();
		for (Object x : a.getCollection()) {
			tc.add(GenObjConverter.convert(x));
		}
		GenericCollection gc = new GenericCollection(a.getElClzName());
		gc.setCollection(tc);
		return gc;
	}

	@Override
	public RpcGenCollection convertAB(GenericCollection a) throws Exception {
		return convert(a);
	}

	@Override
	public GenericCollection convertBA(RpcGenCollection b) throws Exception {
		return convert(b);
	}

}
