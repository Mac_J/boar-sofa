package com.boarsoft.sofa.converter;

import java.io.IOException;

import com.alipay.hessian.generic.model.GenericArray;
import com.alipay.hessian.generic.model.GenericCollection;
import com.alipay.hessian.generic.model.GenericMap;
import com.alipay.hessian.generic.model.GenericObject;
import com.boarsoft.bean.ObjectConverter;
import com.boarsoft.rpc.generalize.RpcGenArray;
import com.boarsoft.rpc.generalize.RpcGenCollection;
import com.boarsoft.rpc.generalize.RpcGenMap;
import com.boarsoft.rpc.generalize.RpcGenObj;

public class GenObjConverter implements ObjectConverter<GenericObject, RpcGenObj> {

	public static RpcGenObj convert(GenericObject a) throws Exception {
		if (a == null) {
			return null;
		}
		return new RpcGenObj(a.getType(), a.getFields());
	}

	public static GenericObject convert(RpcGenObj b) throws IOException {
		if (b == null) {
			return null;
		}
		GenericObject a = new GenericObject(b.getClazzName());
		a.getFields().putAll(b.getMap());
		return a;
	}

	@Override
	public RpcGenObj convertAB(GenericObject a) throws Exception {
		return GenObjConverter.convert(a);
	}

	@Override
	public GenericObject convertBA(RpcGenObj b) throws IOException {
		return GenObjConverter.convert(b);
	}

	public static RpcGenArray convert(GenericArray a) throws Exception {
		return GenArrConverter.convert(a);
	}

	public static GenericArray convert(RpcGenArray a) throws Exception {
		return GenArrConverter.convert(a);
	}

	public static RpcGenCollection convert(GenericCollection a) throws Exception {
		return GenCollConverter.convert(a);
	}

	public static GenericCollection convert(RpcGenCollection a) throws Exception {
		return GenCollConverter.convert(a);
	}

	public static RpcGenMap convert(GenericMap x) throws Exception {
		return GenMapConverter.convert(x);
	}

	public static Object convert(Object x) throws Exception {
		Object y = null;
		if (x == null) {
			return y;
		} else if (x instanceof GenericObject) {
			return GenObjConverter.convert((GenericObject) x);
		} else if (x instanceof GenericArray) {
			return GenArrConverter.convert((GenericArray) x);
		} else if (x instanceof GenericCollection) {
			return GenCollConverter.convert((GenericCollection) x);
		} else if (x instanceof GenericMap) {
			return GenMapConverter.convert((GenericMap) x);
		}
		return x;
	}
}
