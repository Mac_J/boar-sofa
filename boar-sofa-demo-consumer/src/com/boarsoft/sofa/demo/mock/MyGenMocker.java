package com.boarsoft.sofa.demo.mock;

import org.springframework.stereotype.Component;

import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.generalize.RpcGenMocker;
import com.boarsoft.rpc.generalize.RpcGenObj;
import com.boarsoft.rpc.generalize.RpcGenSet;

@Component("myGenMocker")
public class MyGenMocker implements RpcGenMocker {

	@Override
	public Object invoke(RpcMethodConfig rmc, Object[] args) {
		switch (rmc.getSign()) {
		case "hello5(java.util.Set)":
			RpcGenSet set = ((RpcGenSet) args[0]);
			for (Object u : set) {
				System.out.println(u);
			}
			RpcGenObj o = new RpcGenObj("com.boarsoft.rpc.demo.User2");
			o.put("name", "Jenny");
			return new RpcGenObj[] { o };
		default:
			return null;
		}
	}
}
