package com.boarsoft.sofa.test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alipay.sofa.rpc.quickstart.HelloService;
import com.boarsoft.common.util.JsonUtil;
import com.boarsoft.rpc.RpcConfig;
import com.boarsoft.rpc.bean.RpcMethodConfig;
import com.boarsoft.rpc.bean.RpcReferenceConfig;
import com.boarsoft.rpc.generalize.RpcGenInvoker;
import com.boarsoft.rpc.generalize.RpcGenObj;
import com.boarsoft.rpc.generalize.RpcGenSet;
import com.boarsoft.sofa.SofaAdapter;
import com.boarsoft.sofa.demo.DemoService;
import com.boarsoft.sofa.demo.DemoService2;
import com.boarsoft.sofa.demo.User;

public class MainTest {
	private static final Logger log = LoggerFactory.getLogger(MainTest.class);

	private ClassPathXmlApplicationContext ctx;

	@Before
	public void before() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		}
	}

	/**
	 * 通过XML配置引用并调用sofa-rpc-example下的quickstart/HelloService
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void rpcTest() throws InterruptedException {
		for (int i = 0; i < 1; i++) {
			log.info("Invoke rpcTest {}", i);
			DemoService demoService = ctx.getBean(DemoService.class);
			log.info(demoService.hello1(new User("Mac_J"), 18));
			log.info("Invoke rpcTest success ~~~~~~~~{}~~~~~~~~", i);
			Thread.sleep(RpcConfig.HEARTBEAT_PERIOD * 4);
		}
	}

	@Test
	public void sofaTest() throws InterruptedException {
		DemoService2 demoService = ctx.getBean(DemoService2.class);
		log.info("Result = {}", demoService.hello1(new User("Mac_J"), 18));
	}

	/**
	 * 通过XML配置引用并调用sofa-rpc-example下的quickstart/HelloService
	 */
	@Test
	public void helloTest() {
		log.info("Invoke helloService");
		HelloService helloService = ctx.getBean(HelloService.class);
		log.info(helloService.sayHello("Mac_J"));
	}

	/**
	 * 通过api配置引用并调用sofa-rpc-example下的quickstart/HelloService
	 * 
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws TimeoutException 
	 * @throws ExecutionException 
	 */
	@Test
	public void apiTest() throws ClassNotFoundException, InterruptedException, ExecutionException, TimeoutException {
		log.info("Refer and invoke helloService");
		SofaAdapter sofaAdapter = ctx.getBean("sofaAdapter", SofaAdapter.class);
		RpcReferenceConfig rc = new RpcReferenceConfig("sofa", "demo" //
				, "com.alipay.sofa.rpc.quickstart.HelloService", "1.0.0", "helloService");
		rc.setTimeout(3000);
		HelloService helloService = sofaAdapter.referReference(rc, HelloService.class);
		log.info(helloService.sayHello("Mac_J"));
	}
	
	/**
	 * 泛化调用示例
	 * 
	 * @throws Throwable
	 */
	@Test
	public void generializeTest1() throws Throwable {
		RpcGenObj um = new RpcGenObj("com.boarsoft.sofa.demo.User");
		um.put("name", "Mac_J");
		RpcGenSet set = new RpcGenSet("java.util.HashSet");
		set.add(um);
		RpcReferenceConfig rc = new RpcReferenceConfig(//
				"sofa", "demo", "com.boarsoft.sofa.demo.DemoService", "1.0.0", "demoService");
		// rc.setMocker("myGenMocker");
		// rc.setAutoMock(true);
		// rc.setProtocol("sofa");
		RpcMethodConfig rmc = new RpcMethodConfig(rc, "hello5(java.util.Set)");
		Object ro = RpcGenInvoker.invoke(rc, rmc, new Object[] { set });
		log.info(JsonUtil.toJSONString(ro));
	}

	/**
	 * 泛化调用示例
	 * 
	 * @throws Throwable
	 */
	@Test
	public void generializeTest2() throws Throwable {
		RpcGenObj um = new RpcGenObj("com.boarsoft.sofa.demo.User2");
		um.put("name", "Mac_J");
		RpcGenSet set = new RpcGenSet("java.util.HashSet");
		set.add(um);
		RpcReferenceConfig rc = new RpcReferenceConfig(//
				"sofa", "demo", "com.boarsoft.sofa.demo.DemoService2", "1.0.0", "demoService");
		rc.setMocker("myGenMocker");
		// rc.setAutoMock(true);
		rc.setProtocol("sofa");
		RpcMethodConfig rmc = new RpcMethodConfig(rc, "hello5(java.util.Set)");
		Object ro = RpcGenInvoker.invoke(rc, rmc, new Object[] { set });
		log.info(JsonUtil.toJSONString(ro));
	}

}
