package com.boarsoft.sofa.demo;

import java.util.List;
import java.util.Set;

public interface DemoService2 {
	String hello1(User u, int age);

	User hello2(User u);

	List<User> hello3(Set<User> u);

	Set<User> hello4(Set<User> u);

	User[] hello5(Set<User> u);
}
