package com.boarsoft.sofa.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("demoService2")
public class DemoService2Impl implements DemoService2 {
	private static final Logger log = LoggerFactory.getLogger(DemoService2Impl.class);

	@Override
	public String hello1(User u, int age) {
		log.info("hello3 be executed");
		return new StringBuilder().append("Hello ")//
				.append(u.getName()).append(", age = ").append(age).toString();
	}

	@Override
	public User hello2(User u) {
		log.info("hello2 be executed");
		return new User("Jenny");
	}

	@Override
	public List<User> hello3(Set<User> set) {
		log.info("hello3 be executed");
		List<User> lt = new ArrayList<>();
		lt.add(new User("Jenny"));
		for (User u : set) {
			lt.add(u);
		}
		return lt;
	}

	@Override
	public Set<User> hello4(Set<User> set) {
		log.info("hello4 be executed");
		Set<User> lt = new HashSet<>();
		lt.add(new User("Jenny"));
		for (User u : set) {
			lt.add(u);
		}
		return lt;
	}

	@Override
	public User[] hello5(Set<User> set) {
		log.info("hello5 be executed");
		User[] lt = new User[set.size()];
		int i = 0;
		for (User u : set) {
			lt[i++] = u;
		}
		return lt;
	}
}
