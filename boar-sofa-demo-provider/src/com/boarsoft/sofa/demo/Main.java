package com.boarsoft.sofa.demo;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alipay.sofa.rpc.common.RpcConstants;
import com.alipay.sofa.rpc.config.ProviderConfig;
import com.alipay.sofa.rpc.config.RegistryConfig;
import com.alipay.sofa.rpc.config.ServerConfig;

public class Main {
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) throws NumberFormatException, IOException {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
		
//        RegistryConfig registryConfig = new RegistryConfig()
//                .setProtocol(RpcConstants.REGISTRY_PROTOCOL_ZK)
//                .setAddress("127.0.0.1:2181");
//
//        ServerConfig serverConfig = new ServerConfig()
//            .setPort(22101)
//            .setDaemon(false);
//
//        ProviderConfig<DemoService> providerConfig = new ProviderConfig<DemoService>()
//            .setInterfaceId(DemoService.class.getName())
//            .setRef(new DemoServiceImpl())
//            .setServer(serverConfig)
//            .setRegistry(registryConfig);
//
//        providerConfig.export();
        
//        System.out.println("done");
	}
}
